from make_sbom import generate_sbom
import json
import pytest

def test_generate_sbom():
    sbom_content = generate_sbom()
    sbom = json.loads(sbom_content)

    assert "name" in sbom
    assert "version" in sbom
    assert "dependencies" in sbom

    assert sbom["name"] == "JSON Jamboree"
    assert sbom["version"] == "1.0.0"

    dependencies = sbom["dependencies"]
    assert isinstance(dependencies, list)
    assert len(dependencies) == 3

    for dependency in dependencies:
        assert "name" in dependency
        assert "version" in dependency

def test_generate_sbom_format():
    sbom_content = generate_sbom()
    assert sbom_content.startswith("{\n")
    assert '"name": "JSON Jamboree"' in sbom_content
    assert '"version": "1.0.0"' in sbom_content
    assert '"dependencies": [' in sbom_content

if __name__ == "__main__":
    pytest.main(["test_make_sbom.py"])

