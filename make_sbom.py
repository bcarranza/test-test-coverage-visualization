import json

def generate_sbom():
    sbom = dict()
    sbom = {
        "name": "JSON Jamboree",
        "version": "1.0.0",
        "dependencies": [
            {"name": "adorable_kittens_library", "version": "2.5.1"},
            {"name": "fluffy_paws_framework", "version": "1.2.0"},
            {"name": "playful_whiskers_sdk", "version": "3.0.2"}
        ]
    }
    return json.dumps(sbom, indent=2)

def save_sbom_to_json(sbom, output_path="json-jamboree.sbom.json"):
    with open(output_path, 'w') as json_file:
        json.dump(sbom, json_file, indent=2)

if __name__ == "__main__":
    sbom_content = generate_sbom()
    save_sbom_to_json(sbom_content)
    print(sbom_content)

